<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OrderResource\Pages;
use App\Filament\Resources\OrderResource\RelationManagers;
use App\Models\Order;
use App\Models\Product;
use Filament\Forms;
use Filament\Forms\Components\Builder;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Section;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ToggleColumn; 
use Illuminate\Database\Eloquent\SoftDeletingScope;

class OrderResource extends Resource
{
    protected static ?string $model = Order::class;

    protected static ?string $navigationIcon = 'heroicon-o-shopping-bag';

    protected static ?string $navigationGroup = 'Order Managements';
    
    protected static function getNavigationBadge(): ?string
    {
        return static::getModel()::count();
    }
    
    public static function form(Form $form): Form
    { 
        return $form
            ->schema([
                //
                Grid::make(2)
                ->schema([
             
                Card::make([ 
                                    
                    Toggle::make('is_paid')->required(),
     
                    Select::make('customer_id')
                    ->relationship('customer', 'name')
                    ->required(),

                    
                    Select::make('status_id')
                    ->relationship('status', 'name')
                    ->required(),
    
                    TextInput::make('transaction_id') 
                    ->required(),

                    
                    Select::make('payment_method_id')
                    ->relationship('payment_method', 'name')
                    ->required(),
    

     
                    TextInput::make('shipping_fee')
                    ->numeric()
                    ->required(),
    
                    TextInput::make('total_amount')
                    ->numeric() 
                    ->required(), 
     
                    RichEditor::make('shipping_address')->required(), 
                    ])
                ]),
                    Section::make('Products')
                    ->schema([ 
                    Repeater::make('product_list')
                    ->label('')
                    ->createItemButtonLabel('Add Product')
                    ->schema([
                        Select::make('product_list') 
                        ->label('Product')
                        ->options(Product::all()->pluck('name', 'id'))
    
                        ->required(),
                        
                        TextInput::make('quantity')->required(),                    
                        
                        TextInput::make('price')->mask(fn (TextInput\Mask $mask) => $mask->money(prefix: '₱', thousandsSeparator: ',', decimalPlaces: 2)),
    
    
                    ])
                    ->columns(3),
                            ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                TextColumn::make('transaction_id'),
                ToggleColumn::make('is_paid')->disabled(),
                TextColumn::make('products'),
                TextColumn::make('shipping_fee'),
                TextColumn::make('total_amount'),
                TextColumn::make('payment_method.name'), 
                TextColumn::make('customer.name'), 
                BadgeColumn::make('status.name')
                ->color(static function ($state): string {
                    if ($state === 'Completed') {
                        return 'success';
                    }  if ($state === 'Pending') {
                        return 'warning';
                    }if ($state === 'Cancelled' ||$state === 'Failed Delivery' ) {
                        return 'danger';
                    }

                return 'secondary';
                })
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\ViewAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
            // RelationManagers\ProductsRelationManager::class,

        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOrders::route('/'),
            'create' => Pages\CreateOrder::route('/create'),
            'edit' => Pages\EditOrder::route('/{record}/edit'),
        ];
    }    
}
