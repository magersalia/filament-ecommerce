<?php

namespace App\Filament\Resources\OrderResource\Widgets;

use App\Models\Order;
use App\Models\OrderStatus;
use Filament\Widgets\LineChartWidget;
use Illuminate\Support\Facades\DB;

class OrdersChart extends LineChartWidget
{
    protected static ?string $heading = 'Orders';

    protected function getData(): array
    {
        // $labels = OrderStatus::orderBy('order')->withCount('orders')->get();
        $statuses = OrderStatus::orderBy('order')->get();
        $data = [];
        $labels = [];
 
        foreach ($statuses as $key => $status) { 
            $labels[] = $status->name;
            $data[$key]['label'] = $status->name;
            // $data[$key]['status'] = $status->name;
            $data[$key]['data'] = Order::where('status_id',$status->id)->get(); 
            $data[$key]['data2'] = [2,3,5,1,2,3,5,6]; 
            $data[$key]['backgroundColor'] = '#3498db';
            $data[$key]['borderColor'] = '#3498db';
           }
 
 
    

        // $orders = Order::withCount('status')->get();
        // dd($labels);
        // $ordersData = DB::table('order_statuses')
        // ->join('orders', 'order_statuses.id', '=', 'orders.status_id')
        // ->select('order_statuses.name', DB::raw('count(orders.id) as orders_count'))
        // ->groupBy('order_statuses.name', )
        // ->get()->toArray();
     

        // foreach ($ordersData as $key => $orderData) {
        //     $datasets[$key]['label'] = $orderData->name;
        //     $datasets[$key]['data'] = [0, 2, 3, 1, 22, 36, 50, 72, 30, 22, 21, 1];
        //     $datasets[$key]['backgroundColor'] = '#3498db';
        //     $datasets[$key]['borderColor'] = '#3498db';
        // }

        // dd($datasets);
 

        return [
            'datasets' =>  
            // [
            //     [
            //         'label' => 'Pending',
            //         'backgroundColor'=>'#3498db',
            //         'borderColor'=>'#3498db',
            //         'data' => [0, 2, 3, 1, 22, 36, 50, 72, 30, 22, 21, 1],
                    
                    
            //     ],     [
            //         'label' => 'Products deleted',
            //         // 'data' => [0, 2, 3, 1, 22, 36, 50, 72, 30, 22, 21, 1],
            //         'backgroundColor'=>'#e74c3c',
            //         'borderColor'=>'#e74c3c',
            //     ], 
            // ],
            $data,
            'labels' => $labels,
        ];
    }
}
