<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ProductResource\Pages;
use App\Filament\Resources\ProductResource\RelationManagers;
use App\Filament\Resources\ProductResource\Widgets\ProductOverview;
use App\Filament\Resources\ProductResource\Widgets\ProductStatsOverview;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Filament\Forms;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Forms\Components\Wizard;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Columns\ToggleColumn;
use Filament\Tables\Filters\Filter;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Illuminate\Support\HtmlString;

class ProductResource extends Resource
{
    protected static ?string $model = Product::class;
 
    protected static ?string $navigationIcon = 'heroicon-o-shopping-cart';
  
    protected static ?string $navigationGroup = 'Product Managements';

    protected static ?int $navigationSort = 1;

    protected static function getNavigationBadge(): ?string
{
    return static::getModel()::count();
}


    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make(1)
                ->schema([
                    
                Wizard::make([
                    Wizard\Step::make('Details')
                    ->schema([
                        Card::make([

                            Select::make('category_id')
                            ->relationship('category', 'name')
                                ->preload()
                                ->required(),

                            TextInput::make('name')->required(),
                            TextInput::make('price')->mask(fn (TextInput\Mask $mask) => $mask->money(prefix: '₱', thousandsSeparator: ',', decimalPlaces: 2)),

                            TextInput::make('quantity')
                            ->numeric()
                                ->minValue(1)
                                ->maxValue(100)
                                ->required(),  

                            Toggle::make('is_forsale')->label('For Sale')->required(),
                            Toggle::make('featured')->required(),

                        ]), 
                    ]),
                    Wizard\Step::make('Desciption')
                    ->schema([

                        Card::make([
                            RichEditor::make('description')->required(),
                        ])

                    ]),
                    Wizard\Step::make('Images')
                        ->schema([
                            Card::make()
                                ->schema([


                                    FileUpload::make('images')
                                        ->label('Images')
                                        ->image()
                                        ->imageResizeMode('cover')
                                        ->preserveFilenames()
                                        ->multiple()
                                        ->enableReordering()
                                        ->enableDownload()
                                        ->enableOpen()

                                        ->minFiles(1)
                                        ->maxFiles(4),
                                ])
                        ]),
                ])

            ])
            
        ]);
    }

    public static function table(Table $table): Table
    {
 
        return $table
            ->columns([
                // 
                ImageColumn::make('cover')->square(),
                TextColumn::make('name')->limit(30),
                TextColumn::make('price'), // Add a separator for decimal numbers.,
                TextColumn::make('category.name'),
                TextColumn::make('quantity'),
                ToggleColumn::make('featured'),
                ToggleColumn::make('is_forsale'),

            ])
            ->filters([
                //
                 
                Filter::make('featured')
                ->query(fn (Builder $query): Builder => $query->where('featured', true)),

                Filter::make('is_forsale')
                ->query(fn (Builder $query): Builder => $query->where('is_forsale', true)),

                SelectFilter::make('category')->relationship('category', 'name')
                ->multiple(),


                 
                Filter::make('created_at')
                ->form([
                    Forms\Components\DatePicker::make('created_from'),
                    Forms\Components\DatePicker::make('created_until'),
                ])

                ->query(function (Builder $query, array $data): Builder {
                    return $query
                        ->when(
                            $data['created_from'],
                            fn (Builder $query, $date): Builder => $query->whereDate('created_at', '>=', $date),
                        )
                        ->when(
                            $data['created_until'],
                            fn (Builder $query, $date): Builder => $query->whereDate('created_at', '<=', $date),
                        );
                })
            ])
            ->actions([
                Tables\Actions\ViewAction::make(),

                Tables\Actions\EditAction::make(), 
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListProducts::route('/'),
            'create' => Pages\CreateProduct::route('/create'),
            'edit' => Pages\EditProduct::route('/{record}/edit'),
        ];
    }    

    public static function getWidgets(): array
    {
        return [
            ProductStatsOverview::class    
        ];
    }

}
