<?php

namespace App\Filament\Resources\ProductResource\Pages;

use App\Filament\Resources\ProductResource;
use App\Filament\Resources\ProductResource\Widgets\ProductOverview;
use App\Filament\Resources\ProductResource\Widgets\ProductStatsOverview;
use App\Models\Product;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Database\Eloquent\Builder;

class ListProducts extends ListRecords
{
    protected static string $resource = ProductResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

     
public function isTableSearchable(): bool
{
    return true;
}
 
protected function applySearchToTableQuery(Builder $query): Builder
{
    if (filled($searchQuery = $this->getTableSearchQuery())) {
        //In this example a filter scope on the model is used
        //But you can also customize the query right here!
        return $query->filter(['search' => $searchQuery]);
    }
 
    return $query;
}
protected function getHeaderWidgets(): array
{
    return [
        ProductStatsOverview ::class,
    ];
}
}
