<?php

namespace App\Filament\Resources\ProductResource\Widgets;

use App\Models\Product;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

class ProductStatsOverview extends BaseWidget
{
    protected function getCards(): array
    {
        return [
            //
            
            Card::make('Total Products', Product::count()),
            Card::make('Featured', Product::where('featured',1)->count()),
            Card::make('For sale', Product::where('is_forsale',1)->count()),
        ];
    }
}
