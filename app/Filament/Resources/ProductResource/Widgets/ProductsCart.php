<?php

namespace App\Filament\Resources\ProductResource\Widgets;

use App\Models\Product;
use Filament\Widgets\BarChartWidget;
use Filament\Widgets\LineChartWidget;
use Illuminate\Support\Facades\DB;

class ProductsCart extends LineChartWidget
{
    protected static ?string $heading = 'Chart';
    protected function getHeading(): string
    {
        return 'Products';
    }
 
    protected function getData(): array
    {
  

       $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $data = [];
       foreach ($months as $key => $month) {
        $data[$month] = Product::countByMonth($key + 1); 
       }

 
        return [
            'datasets' => [
                [
                    'label' => 'Products created',
                    'backgroundColor'=>'#3498db',
                    'borderColor'=>'#3498db',
                    'data' => $data,
                ],     [
                    'label' => 'Products deleted',
                    'data' => [0, 2, 3, 1, 22, 36, 50, 72, 30, 22, 21, 1],
                    'backgroundColor'=>'#e74c3c',
                    'borderColor'=>'#e74c3c',
                ],
            ],
            // 'labels' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        ];
    }
}
