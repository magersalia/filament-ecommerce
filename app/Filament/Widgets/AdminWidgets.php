<?php

namespace App\Filament\Widgets;

use App\Models\Category;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

class AdminWidgets extends BaseWidget
{
    protected function getCards(): array
    {
        return [
            //
            Card::make('Total Products', Product::count())
            ->description('32k increase')
            ->descriptionIcon('heroicon-s-trending-up'),
            Card::make('Orders',  Order::count())
            ->description('7% increase')
            ->descriptionIcon('heroicon-s-trending-down'),
            Card::make('Total Users',User::count())
            ->description('3% increase')
            ->descriptionIcon('heroicon-s-trending-up'),
        ];
    }
}
