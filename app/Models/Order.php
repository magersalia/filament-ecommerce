<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['customer_id','is_paid','shipping_fee','total_amount','shipping_address','transaction_id','payment_method_id','status_id','product_list'];

    protected $casts = [
        'product_list' => 'array',
    ];

    protected $appends = ['products'];

    public function customer(){
        return $this->belongsTo(User::class,'customer_id');
    }

    public function status(){
        return $this->belongsTo(OrderStatus::class,'status_id');

    }

    public function payment_method(){
        return $this->belongsTo(PaymentMethod::class);
    }
 
    public function order_products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function getProductsAttribute(){
        return $this->product_list ? count($this->product_list) : 0;
    }
}
