<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    use HasFactory;

     

    protected $fillable = ['name','description','price','quantity','featured','images','cover_image','category_id','is_forsale'];

    protected $appends = ['cover'];

    protected $casts = [
        'images' => 'array',
    ];
    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function getCoverAttribute()
    {
        return $this->images ?$this->images[0] : null;
    }
    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? null, function ($query, $search) {
            $query->where('name', 'like', '%'.$search.'%')
            ->orWhere('description', 'like', '%'.$search.'%')
            ->orWhereHas('category', function($q) use ($search){
                $q->where('name',$search);
            })
            // ->orWhere('category', 'like', '%'.$search.'%')
                ;
        }) ;
    }

    public function scopeCountByMonth($q,$m){
        return $q->whereMonth('created_at', '=', $m)->count();
    }
 


}

